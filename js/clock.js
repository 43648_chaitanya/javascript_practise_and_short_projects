console.log('clock ');


function updateClock(){
    let currentTime = new Date();

    let currentHour = currentTime.getHours();
    let currentMinutes = currentTime.getMinutes();

    let currentSec = currentTime.getSeconds();

    currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes ;
    currentSec = (currentSec < 10 ? "0" : "") + currentSec ;


    let day = (currentTime > 12) ? "PM" : "AM" ;

    let strTime = currentHour + ":" + currentMinutes + ":" + currentSec  + " " + day ;

    document.getElementById("clock").innerHTML = strTime;



}