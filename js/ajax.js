console.log('thisis ajax practise');

let fetchBtn = document.getElementById('fetchBtn');
let backupBtn = document.getElementById('backupBtn');

fetchBtn.addEventListener('click',buttonClickHandler);

function buttonClickHandler(){

    const xhr = new XMLHttpRequest();

    xhr.open('GET','harry.txt',true);

    xhr.onprogress = function(){
        console.log('on progresss ');
    }

    xhr.onload = function(){
        console.log(this.responseText);
    }
    
    xhr.send();
}