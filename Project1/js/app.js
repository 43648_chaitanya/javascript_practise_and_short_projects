console.log('welcome to project');
showNotes();

// if user adds a note add it to local storage 

let addBtn = document.getElementById('addBtn');

addBtn.addEventListener("click",function(e){

    let addTxt = document.getElementById("addTxt");
    let addTitle = document.getElementById("addTitle");

    let notes = localStorage.getItem("notes");
    

    if(notes == null){
        notesObj = [];
    }
    else{
        notesObj = JSON.parse(notes);
    }
    let myObj = {
        title : addTitle.value,
        text : addTxt.value
    }

    notesObj.push(myObj);
    localStorage.setItem("notes",JSON.stringify(notesObj));
    addTxt.value= "";
    addTitle.value="";

    // console.log(notesObj);

    showNotes();
})

//get it from local storage and put in display
function showNotes(){
    let notes = localStorage.getItem("notes");
    if(notes == null){
        notesObj = [];
    }
    else{
        notesObj = JSON.parse(notes);
    }
    let html = "";
    notesObj.forEach(function(element,index){
        html += `
        <div class="noteCard my-2 mx-2 card" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">${element.title}</h5>
                  <p class="card-text">${element.text}</p>
                  <button id="${index}" onclick="deleteNote(this.id)" class="btn btn-primary">Delete Note</button>
                </div>
              </div>`
    })

    let noteElem =  document.getElementById("notes");
    if(notesObj.length != 0){
        noteElem.innerHTML = html;
    }
    else{
        noteElem.innerHTML = `<h3>You dont have notes here </h3>`;
    }

}

//function to delete the note 

function deleteNote(index){
// console.log(index);
// console.log('i am deleting ');

let notes = localStorage.getItem("notes");
    if(notes == null){
        notesObj = [];
    }
    else{
        notesObj = JSON.parse(notes);
    }

    notesObj.splice(index,1);
    localStorage.setItem("notes",JSON.stringify(notesObj));
    showNotes();

}

let search = document.getElementById('searchTxt');

search.addEventListener("input",function(e){

    let inputValue = search.value.toLowerCase();

    let noteCard= document.getElementsByClassName("noteCard");

    Array.from(noteCard).forEach(function(element){
        let txt = element.getElementsByTagName("p")[0].innerText;

        if(txt.includes(inputValue)){
            element.style.display = "block";
        }
        else{
            element.style.display = "none";
        }
    })
})